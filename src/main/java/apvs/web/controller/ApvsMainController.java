package apvs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import apvs.web.service.ApvsService;
import apvs.web.util.ApkInfoTransformer;

/**
 * @author Carl Adler (C.A.)
 * */
@Controller
public class ApvsMainController {
	
	@Autowired
	ApvsService apvsService;

	@RequestMapping(value = "/verifyapkfile", method = RequestMethod.POST)
	public @ResponseBody String verifyApkFile(@RequestBody LinkedMultiValueMap<String, String> unverifiedApkInfo){
		return apvsService.verifyApkIntegrity(ApkInfoTransformer.transformReqMapIntoAplInfoObj(unverifiedApkInfo));
	}
	
}
