package apvs.web.util;

/**
 * @author Carl Adler (C.A.)
 * */
public class APVSConstantDescriptor {
	
	public static final String TARGET_KEY = "SHA1-Digest";
	public static final String ANDROID_MANIFEST_XML = "AndroidManifest.xml";
	public static final String RESOURCES_ARSC = "resources.arsc";
	public static final String CLASSES_DEX = "classes.dex";
	public static final String ANDROID_VERSION_CODE = "android:versionCode";
	
	public static final int RESULT_CODE_FOR_PASSED = 1;
	public static final int RESULT_CODE_FOR_VERSION_MISMATCH = 2;
	public static final int RESULT_CODE_FOR_UNKNOWN = 3;
	public static final int RESULT_CODE_FOR_DANGEROUS = 4;
	
	public static final String LIGHT_PASSED = "green";
	public static final String LIGHT_DANGEROUS = "red";
	public static final String LIGHT_UNKNOWN = "yellow";
	public static final String LIGHT_VERSION_MISMATCH = "blue";
	
	public static final String MESSAGE_FOR_PASSED = "Successful.";
	public static final String MESSAGE_FOR_DANGEROUS = "Failed.";
	public static final String MESSAGE_FOR_UNKNOWN = "Unknown, no matched apk samples in database.";
	public static final String MESSAGE_FOR_VERSION_MISMATCH = "Version mismatch, please update the apk to the latest version.";
	
}
