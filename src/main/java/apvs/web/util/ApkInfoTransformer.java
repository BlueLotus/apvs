package apvs.web.util;

import org.springframework.util.LinkedMultiValueMap;

import apvs.web.dto.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public class ApkInfoTransformer {
	
	public static APKManifest transformReqMapIntoAplInfoObj(LinkedMultiValueMap<String, String> dataSet) {
		APKManifest apkManifest = new APKManifest();
		apkManifest.setApkFileName(dataSet.getFirst("apkFileName"));
		apkManifest.setQueryIdentifier(dataSet.getFirst("queryIdentifier"));
		apkManifest.setShaForAndroidManifestXml(dataSet.getFirst("androidManifestXml"));
		apkManifest.setShaForResourcesArsc(dataSet.getFirst("resourcesArsc"));
		apkManifest.setShaForClassesDex(dataSet.getFirst("classesDex"));
		apkManifest.setMnftEntrySize(Integer.parseInt(dataSet.getFirst("mnftEntrySize")));
		apkManifest.setVersionCode(dataSet.getFirst("versionCode"));
		return apkManifest;
	}
	
}
