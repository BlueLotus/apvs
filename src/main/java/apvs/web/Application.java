package apvs.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Carl Adler (C.A.)
 * */
@Configuration
@ComponentScan
@EnableAutoConfiguration
@ImportResource("application-context.xml")
public class Application {
    public static void main(String[] args) {    	
    	SpringApplication.run(Application.class, args);
    }
}
