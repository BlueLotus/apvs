package apvs.web.service;

import java.util.List;

import apvs.web.dto.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public interface ApvsService {
	
	public List<APKManifest> obtainAllAplSamplesInDB();
	
	public String verifyApkIntegrity(APKManifest apkManifest);
	
}
