package apvs.web.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import apvs.web.dao.ApkVerificationDao;
import apvs.web.dto.APKManifest;
import apvs.web.service.ApvsService;
import apvs.web.util.APVSConstantDescriptor;
import apvs.web.util.ApkMetaInfoComparator;

/**
 * @author Carl Adler (C.A.)
 * */
@Service
public class ApvsServiceImpl implements ApvsService{
	
	private static final Logger logger = Logger.getLogger(ApvsServiceImpl.class);

	private ApkVerificationDao apkVerificationDao;

	public void setApkVerificationDao(ApkVerificationDao apkVerificationDao) {
		this.apkVerificationDao = apkVerificationDao;
	}
	
	@Override
	public List<APKManifest> obtainAllAplSamplesInDB() {
		return apkVerificationDao.getAllApkInfo();
	}

	@Override
	public String verifyApkIntegrity(APKManifest unverified) {
		
		String verifyResult = APVSConstantDescriptor.LIGHT_DANGEROUS;
		String verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_DANGEROUS;
		APKManifest verified = null;
		
		try {			
			 verified = apkVerificationDao.getSpecificApkInfoWithFileName(unverified.getApkFileName());
		} catch (Exception e) {
			if(e.getMessage().contains("Incorrect result size: expected 1, actual 0")) {
				logger.debug("No matched apk in db.");
			}
		}
		
		if(verified == null) {
			
			verifyResult = APVSConstantDescriptor.LIGHT_UNKNOWN;
			verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_UNKNOWN;
			
		} else {
			
			int resultCode = ApkMetaInfoComparator.compareApkManifestInfo(unverified, verified);
			
			if(resultCode == APVSConstantDescriptor.RESULT_CODE_FOR_PASSED){
				verifyResult = APVSConstantDescriptor.LIGHT_PASSED;
				verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_PASSED;
			} else if(resultCode == APVSConstantDescriptor.RESULT_CODE_FOR_VERSION_MISMATCH) {
				verifyResult = APVSConstantDescriptor.LIGHT_VERSION_MISMATCH;
				verifyMessage = APVSConstantDescriptor.MESSAGE_FOR_VERSION_MISMATCH;
			}
			
		} 
		
		logger.debug("Apk integrity verification result: " + verifyMessage);
		
		logger.debug("Saving verification result to db...");
		apkVerificationDao.saveVerificationResult(unverified.getQueryIdentifier(), verifyResult);		
		logger.debug("Verification result saved completely.");
		
		return verifyResult;
		
	}

}
