package apvs.web.dao;

import java.util.List;

import apvs.web.dto.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public interface ApkVerificationDao {

	public List<APKManifest> getAllApkInfo();
	
	public APKManifest getSpecificApkInfoWithFileName(String apkFileName);
	
	public String queryVerificationResultByQueryIdentifier(String queryIdentifier);
	
	public boolean saveVerificationResult(String queryIdentifier, String result);
	
}
