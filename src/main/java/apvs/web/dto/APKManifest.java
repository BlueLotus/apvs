package apvs.web.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author Carl Adler (C.A.)
 * 
 * */
public @Data class APKManifest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String queryIdentifier;
	private String apkFileName;
	private String shaForAndroidManifestXml;
	private String shaForResourcesArsc;
	private String shaForClassesDex;
	private int mnftEntrySize;
	private String versionCode;
	
}
