package apvs.web.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import apvs.web.dao.ApkVerificationDao;
import apvs.web.dto.APKManifest;
import apvs.web.service.ApvsService;
import apvs.web.util.APVSConstantDescriptor;

/**
 * @author Carl Adler (C.A.)
 * */
public class ApkVerificationTest {

	static GenericXmlApplicationContext context;
	static ApvsService apvsService;
	static ApkVerificationDao apkVerificationDao;
	static APKManifest unverifiedApkForNormal;
	static APKManifest unverifiedApkMnftForVersionMismatched;
	static APKManifest unverifiedApkMnftForUnknown;
	static APKManifest unverifiedApkMnftForRepackaged;
	static String queryIdentifierForRed;
	static String queryIdentifierForYellow;
	static String queryIdentifierForGreen;
	static String queryIdentifierForBlue;
	static String queryIdentifierForRWTest;
	
	@BeforeClass
	public static void setUp() throws Exception {
		context = new GenericXmlApplicationContext();
		context.load("classpath:test-app-context.xml");
		context.refresh();
		
		apvsService = context.getBean("apvsService", ApvsService.class);
		apkVerificationDao = context.getBean("apkVerificationDao", ApkVerificationDao.class);
		
		unverifiedApkForNormal = new APKManifest();
		unverifiedApkForNormal.setQueryIdentifier("IMEI3999257020141028flipboard.apk");
		unverifiedApkForNormal.setApkFileName("com.evernote.apk");
		unverifiedApkForNormal.setShaForAndroidManifestXml("PbJ/4ebQAD3LRu2ruzeA6noTsZg=");
		unverifiedApkForNormal.setShaForResourcesArsc("VPH6THNDJBerQwdg9WIQ0t29Pyo=");
		unverifiedApkForNormal.setShaForClassesDex("dZbY9kKEvpalBYoyccAKUnYeTp8=");
		unverifiedApkForNormal.setMnftEntrySize(3883);
		unverifiedApkForNormal.setVersionCode("1060113");
		
		unverifiedApkMnftForVersionMismatched = new APKManifest();
		unverifiedApkMnftForVersionMismatched.setQueryIdentifier("IMEI3061288820141028flipboard.apk");
		unverifiedApkMnftForVersionMismatched.setApkFileName("com.rovio.angrybirds.apk");
		unverifiedApkMnftForVersionMismatched.setShaForAndroidManifestXml("Og8kcIbTh6KgJG3z95Idfh/Tk94=");
		unverifiedApkMnftForVersionMismatched.setShaForResourcesArsc("ypmdbezg3o41hdKJTokhdhKr0Wo=");
		unverifiedApkMnftForVersionMismatched.setShaForClassesDex("YWRz8XleuxKs+YaDKnPJs3jpTrY=");
		unverifiedApkMnftForVersionMismatched.setMnftEntrySize(2789);
		unverifiedApkMnftForVersionMismatched.setVersionCode("3210");
		
		unverifiedApkMnftForUnknown = new APKManifest();
		unverifiedApkMnftForUnknown.setQueryIdentifier("IMEI3061777020141028flipboard.apk");
		unverifiedApkMnftForUnknown.setApkFileName("com.evernote2.apk");
		unverifiedApkMnftForUnknown.setShaForAndroidManifestXml("PbJ/4ebQAD3LRu2ruzeA6noTsZg=");
		unverifiedApkMnftForUnknown.setShaForResourcesArsc("VPH6THNDJBerQwdg9WIQ0t29Pyo=");
		unverifiedApkMnftForUnknown.setShaForClassesDex("dZbY9kKEvpalBYoyccAKUnYeTp8=");
		unverifiedApkMnftForUnknown.setMnftEntrySize(3883);
		unverifiedApkMnftForUnknown.setVersionCode("1060113");
		
		unverifiedApkMnftForRepackaged = new APKManifest();
		unverifiedApkMnftForRepackaged.setQueryIdentifier("IMEI3048967020141028flipboard.apk");
		unverifiedApkMnftForRepackaged.setApkFileName("com.evernote.apk");
		unverifiedApkMnftForRepackaged.setShaForAndroidManifestXml("PbJ/4ebQAD3LRu2ruzeA6noTsZ1=");
		unverifiedApkMnftForRepackaged.setShaForResourcesArsc("VQH6THNDJBerQwdg9WIQ0t29Pyo=");
		unverifiedApkMnftForRepackaged.setShaForClassesDex("dAbY9kKEvpalBYoyccAKUnYeTp8=");
		unverifiedApkMnftForRepackaged.setMnftEntrySize(3883);
		unverifiedApkMnftForRepackaged.setVersionCode("1060113");
		
		queryIdentifierForRed = "IMEI3061257020141028flipboard.apk";
		queryIdentifierForYellow = "IMEI2081242020141029angrybird.apk";
		queryIdentifierForGreen = "IMEI8051333020141030gmail.apk";
		queryIdentifierForBlue = "IMEI1551776020141031evernote.apk";
		queryIdentifierForRWTest = "IMEI9021257020141101googlechrome.apk";
	}
	
	@Test
	public void testForListAllApkSamplesInDB() {
		listAllApkSamples(apvsService.obtainAllAplSamplesInDB());
	}

	@Test
	public void testForVerifyNormalApkFile() {
		assertEquals("green", apvsService.verifyApkIntegrity(unverifiedApkForNormal));
	}
	
	@Test
	public void testForVerifyVersionMismatchedApkFile() {
		assertEquals("blue", apvsService.verifyApkIntegrity(unverifiedApkMnftForVersionMismatched));
	}
	
	@Test
	public void testForVerifyUnknownApkFile() {
		assertEquals("yellow", apvsService.verifyApkIntegrity(unverifiedApkMnftForUnknown));
	}
	
	@Test
	public void testForVerifyRepackagedApkFile() {
		assertEquals("red", apvsService.verifyApkIntegrity(unverifiedApkMnftForRepackaged));
	}
	
	@Test
	public void testForCorrectnessOfVerificationResult() {
		assertEquals("red", apkVerificationDao.queryVerificationResultByQueryIdentifier(queryIdentifierForRed));
		assertEquals("yellow", apkVerificationDao.queryVerificationResultByQueryIdentifier(queryIdentifierForYellow));
		assertEquals("green", apkVerificationDao.queryVerificationResultByQueryIdentifier(queryIdentifierForGreen));
		assertEquals("blue", apkVerificationDao.queryVerificationResultByQueryIdentifier(queryIdentifierForBlue));
	}
	
	@Test
	public void testReadAndWriteFunctionalitiesForVerificationResult() {
		String result = "green";
		assertTrue(apkVerificationDao.saveVerificationResult(queryIdentifierForRWTest, result));
		assertEquals(result, apkVerificationDao.queryVerificationResultByQueryIdentifier(queryIdentifierForRWTest));
	}
	
	private void listAllApkSamples(List<APKManifest> apkSampleList) {
		for (APKManifest apkManifest : apkSampleList) {
			System.out.printf("	APK smaple id: %d\n", apkManifest.getId());
			System.out.printf("	APK file name: %s\n", apkManifest.getApkFileName());
			System.out.printf("	Size of entry map in MANIFEST.MF: %s\n", apkManifest.getMnftEntrySize());
			System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.ANDROID_MANIFEST_XML, apkManifest.getShaForAndroidManifestXml());
			System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.RESOURCES_ARSC, apkManifest.getShaForResourcesArsc());
			System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.CLASSES_DEX, apkManifest.getShaForClassesDex());
			System.out.printf("	SHA1 for %s: %s\n", APVSConstantDescriptor.ANDROID_VERSION_CODE, apkManifest.getVersionCode());
		}
	}

}
